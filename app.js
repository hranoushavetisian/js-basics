// function add(num1) {
//     let sum = num1;
//     function mainAdd(num2) {
//         sum += num2;
//         return mainAdd;
//     };
//     mainAdd.toString = function() {
//        return sum;
//     }
//     return mainAdd;
// }
//  alert(add(5)(6));
//  alert(add(7)(3));
//  alert(add(5)(15));
//  alert(add(1)(4));

function add(num1) {
    return function(num2) {
        return num1 + num2
    }
    
}
console.log(add(1)(2));
console.log(add(5)(7));
console.log(add(8)(14));
console.log(add(7)(3));



function avgAge (arr) {
    let num = 0;
    for (let i = 0; i < arr.length; ++i) {
        num += arr[i].age;
    }
    num = num / arr.length;
    return num
}

const users = [
    {
        gender: 'male',
        age: 24,
    },
    {
        gender: 'female',
        age: 21,
    },
    {
        gender: 'male',
        age: 36,
    },
];

console.log(avgAge(users));

function findUniqueElements(arr) {
    let newArr = [];
    for (let i = 0; i < arr.length; ++i) {
        if (!newArr.includes(arr[i])) {
            newArr.push(arr[i]);
        } else if (newArr.includes(arr[i])){ 
            newArr.splice(newArr.indexOf(arr[i]), 1) 
        }
    }

    return newArr
}

console.log(findUniqueElements([5, 6, 7, 5, 8, 6, 11]));


function createCounter() {
  
    function getCounter() {
      return getCounter.count++;
    };
  
    getCounter.count = 0;
  
    return getCounter;
  }
  
  let getCounter = createCounter();
//   alert( getCounter() );
//   alert( getCounter() );
//   alert( getCounter() );
//   alert( getCounter() );
//   alert( getCounter() );